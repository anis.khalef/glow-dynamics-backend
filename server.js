const express = require("express");
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

const { sendContact } = require('./sendContact.js');
const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  requireTLS: true,
  auth: {
    user: "glowdynamicsllc@gmail.com",
    pass: "qsfxelnqdilireye",
  }
});


app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', ['*', 'https://www.glowdynamics.us/']);
  next();
});

const corsOpts = {
  origin: '*'
};

app.use(cors(corsOpts));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.status(200).send('Express On Vercel!');
});

app.post('/api/contact', (req, res) => {
 
  const { email, subject } = req.body;
try{
  let mailOptions = {
    from: 'Client',
    to: 'glowdynamicsllc@gmail.com',
    subject: 'New Contact',
    html: `<p><b>Client's email is:</b> ${email}</p> <br></br> <b>Subject is: </b> ${subject} `
  };
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
      return res.status(400).send('Mailer Error!');
    }
    res.status(200).send('Email Sent!');
    console.log('Message sent: %s', info.messageId);
  });
  
  res.status(200).send('Contact saved and email sent !');
}
catch(err){
    res.status(500).send('Error !');
 }  
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log("Listening on ", port);
});

module.exports = app;
